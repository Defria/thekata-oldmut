﻿using System;

namespace TheKataTests.TestHelpers;

public static class Helper
{
    public static char SeedRandomChars()
    {
        const string chars = "abcd\n";
        var random = new Random();
        var charArray = chars.ToCharArray();
        var value = random.Next(0, charArray.Length);
        return charArray[value];
    }
}