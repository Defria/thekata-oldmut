﻿using System;
using System.IO;
using System.Linq;
using Moq;
using TheKata.Abstraction;
using TheKata.Implementation;
using TheKataTests.TestDoubles;
using TheKataTests.TestHelpers;
using Xunit;

namespace TheKataTests.ImplementationTests;

public class CopierTests
{
    private readonly Mock<IDestination> _mockIDestination;
    private readonly Mock<ISource> _mockISource;

    public CopierTests()
    {
        _mockISource = new Mock<ISource>();
        _mockIDestination = new Mock<IDestination>();
    }


    [Theory]
    [Trait("UnitTest", "Copier")]
    [InlineData('\n')]
    public void Copy_WhenCalledWithNewLineChar_WriteCharShouldNotBeCalled(char c)
    {
        // arrange
        _mockISource.Setup(m => m.ReadChar()).Returns(c);
        _mockIDestination.Setup(m => m.WriteChar(It.IsAny<char>()));
        var copier = new Copier(_mockISource.Object, _mockIDestination.Object);

        // act
        copier.Copy();

        // assert
        _mockIDestination.Verify(m => m.WriteChar(It.IsAny<char>()), Times.Never);
    }


    [Theory]
    [InlineData('\n', 1)]
    [Trait("UnitTest", "Copier")]
    public void Copy_WhenCalledWithNewLineCharOnly_ReadCharShouldBeCalledOnce(char c, int executionCount)
    {
        // arrange
        _mockISource.Setup(m => m.ReadChar()).Returns(c);
        _mockIDestination.Setup(m => m.WriteChar(It.IsAny<char>()));
        var copier = new Copier(_mockISource.Object, _mockIDestination.Object);

        // act
        copier.Copy();

        // assert
        _mockISource.Verify(m => m.ReadChar(), Times.AtLeast(executionCount));
    }


    [Fact]
    [Trait("UnitTest", "Copier")]
    public void Copy_WhenCalled_ShouldExecuteUntilNewLineCharEncountered()
    {
        // arrange
        _mockISource.Setup(m => m.ReadChar()).Returns(Helper.SeedRandomChars);
        _mockIDestination.Setup(m => m.WriteChar(It.IsAny<char>()));
        var copier = new Copier(_mockISource.Object, _mockIDestination.Object);

        // act
        copier.Copy();

        // assert
        _mockISource.Verify(m => m.ReadChar(), Times.AtLeast(1));
    }

    [Theory]
    [Trait("UnitTest", "Copier")]
    [InlineData(new[] { 'a', 'b', 'c', 'd', '\n' })]
    public void CopyMultiple_WhenCalled_ShouldExecuteUntilNewLineCharEncountered(char[] chars)
    {
        // arrange
        _mockISource.Setup(m => m.ReadChars(10)).Returns(chars);
        _mockIDestination.Setup(m => m.WriteChars(It.IsAny<char[]>()));
        var copier = new Copier(_mockISource.Object, _mockIDestination.Object);

        // act
        copier.Copy(10);

        // assert
        _mockISource.Verify(m => m.ReadChars(It.IsAny<int>()), Times.AtLeast(1));
    } 
    
    [Theory]
    [Trait("UnitTest", "Copier")]
    [InlineData('a')]
    public void Copy_WhenCalledWithoutTerminatingChar_ShouldTimeout(char @char)
    {
        // arrange
        _mockISource.Setup(m => m.ReadChar()).Returns(@char);
        _mockIDestination.Setup(m => m.WriteChar(It.IsAny<char>()));
        var copier = new Copier(_mockISource.Object, _mockIDestination.Object);

        // act

        // assert
        Assert.Throws<TimeoutException>(()=> copier.Copy());
    }

    [Fact]
    [Trait("IntegrationTest", "Copier")]
    public void Copy_WhenCalledWithChars_OutputFileShouldNotBeEmpty()
    {
        // arrange
        var fakeSource = new FakeSource();
        var fakeDestination = new FakeDestination();
        var copier = new Copier(fakeSource, fakeDestination);

        // act
        copier.Copy();
        var output = File.ReadLines(FakeDestination.CopierOldMutTestOutput);

        // Asset
        Assert.True(output.Last().ToCharArray()[0] != '\n');
    }


    [Fact]
    [Trait("IntegrationTest", "Copier")]
    public void CopyMultiple_WhenCalledWithChars_OutputFileShouldNotBeEmpty()
    {
        // arrange
        var fakeSource = new FakeSource();
        var fakeDestination = new FakeDestination();
        var copier = new Copier(fakeSource, fakeDestination);

        // act
        copier.Copy(10);
        var output = File.ReadLines(FakeDestination.CopierOldMutTestOutput);

        // Asset
        Assert.True(output.Last().ToCharArray()[0] != '\n');
    }
}