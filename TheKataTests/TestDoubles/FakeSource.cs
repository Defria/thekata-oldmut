﻿using System.Collections.Generic;
using Castle.Core.Internal;
using TheKata.Abstraction;

namespace TheKataTests.TestDoubles;

public class FakeSource : ISource
{
    private readonly Queue<char> _chars;

    public FakeSource()
    {
        _chars = new Queue<char>();
        const string randomString = "abcdefABCDEF-045$%&*+\n";
        foreach (var str in randomString) _chars.Enqueue(str);
    }

    public char ReadChar()
    {
        return _chars.Dequeue();
    }

    public char[] ReadChars(int count)
    {
        var counter = 0;
        var readCharOutput = new List<char>();
        var @char = _chars.Dequeue();
        while (!_chars.IsNullOrEmpty())
        {
            if (counter > count)
                break;
            readCharOutput.Add(@char);
            @char = _chars.Dequeue();
            counter++;
        }

        return readCharOutput.ToArray();
    }
}