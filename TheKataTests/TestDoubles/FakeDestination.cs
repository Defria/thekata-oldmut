﻿using System.IO;
using TheKata.Abstraction;

namespace TheKataTests.TestDoubles;

public class FakeDestination : IDestination
{
    public static string CopierOldMutTestOutput => "./Copier-OldMut.txt";

    public void WriteChar(char c)
    {
        using var fs = new FileStream(CopierOldMutTestOutput, FileMode.Append, FileAccess.Write);
        using var textWriter = new StreamWriter(fs);
        textWriter.WriteLine(c);
        textWriter.Close();
    }

    public void WriteChars(char[] values)
    {
        using var fs = new FileStream(CopierOldMutTestOutput, FileMode.Append, FileAccess.Write);
        using var textWriter = new StreamWriter(fs);
        textWriter.WriteLine(values);
        textWriter.Close();
    }
}