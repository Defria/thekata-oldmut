﻿namespace TheKata.Abstraction;

public interface ISource
{
    char ReadChar();
    char[] ReadChars(int count);
}