﻿namespace TheKata.Abstraction;

public interface IDestination
{
    void WriteChar(char c);
    void WriteChars(char[] values);
}