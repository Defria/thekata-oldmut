using TheKata.Abstraction;
using TheKata.Implementation;

namespace TheKata.BusinessRules;

public class ConsoleWriter : IDestination
{
    public void WriteChar(char c)
    {
        Console.WriteLine(c);
    }

    public void WriteChars(char[] values)
    {
        Console.WriteLine(string.Join(string.Empty,values));
    }
}