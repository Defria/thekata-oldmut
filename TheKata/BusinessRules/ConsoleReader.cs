using TheKata.Abstraction;

namespace TheKata.BusinessRules;

public class ConsoleReader : ISource
{
    private readonly Queue<char> _chars;

    public ConsoleReader()
    {
        _chars = new Queue<char>();
        const string randomString = "abcdefABCDEF-045$%&*+\n";
        foreach (var str in randomString) _chars.Enqueue(str);
    }

    public char ReadChar()
    {
        return _chars.Dequeue();
    }

    public char[] ReadChars(int count)
    {
        var counter = 0;
        var readCharOutput = new List<char>();
        var @char = _chars.Dequeue();
        while (_chars.Any())
        {
            if (counter > count)
                break;
            readCharOutput.Add(@char);
            @char = _chars.Dequeue();
            counter++;
        }

        return readCharOutput.ToArray();
    }
}