﻿using TheKata.Abstraction;

namespace TheKata.Implementation;

public class Copier
{
    private readonly IDestination _dest;
    private readonly ISource _src;

    public Copier(ISource src, IDestination dest)
    {
        _src = src;
        _dest = dest;
    }

    public void Copy()
    {
        var task = Task.Run(ReadChar);
        const int timeout = 3;
        if (!task.Wait(TimeSpan.FromSeconds(timeout)))
            throw new TimeoutException($"Copy task timeout after {timeout} seconds");
    }

    private void ReadChar()
    {
        const char terminatingChar = '\n';
        var @char = _src.ReadChar();
        if (@char.Equals(terminatingChar)) return;
        _dest.WriteChar(@char);
        ReadChar();
    }

    public void Copy(int count)
    {
        var readChars = _src.ReadChars(count);
        const char terminatingChar = '\n';
        _dest.WriteChars(readChars.TakeWhile(readChar => readChar != terminatingChar).ToArray());
    }
}