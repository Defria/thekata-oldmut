using Autofac;
using TheKata.Abstraction;
using TheKata.BusinessRules;
using TheKata.Implementation;

namespace TheKata.IoC;

public static class Registration
{
    public static IContainer Register()
    {
        var builder = new ContainerBuilder();
        builder.RegisterType<Copier>().AsSelf();
        builder.RegisterType<ConsoleWriter>().As<IDestination>();
        builder.RegisterType<ConsoleReader>().As<ISource>();
        return builder.Build();
    }
}