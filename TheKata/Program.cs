﻿using Autofac;
using TheKata.Implementation;
using TheKata.IoC;

var container = Registration.Register();
using (var scope = container.BeginLifetimeScope())
{
    var app = scope.Resolve<Copier>();
    app.Copy(10);
    app.Copy();
}

Console.WriteLine("Complete");